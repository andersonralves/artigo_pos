Dropzone.options.myDropzone = {
    acceptedFiles: "image/*",
    addRemoveLinks: true,
    maxFiles: 10,
    dictDefaultMessage: "Arraste suas fotos aqui para enviar",
    autoProcessQueue: false
    
};