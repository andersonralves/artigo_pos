<?php


use CoffeeCode\Router\Router;

$router = new Router("http://localhost:8081/public/index.php");

$router->get("/", function() {
    responseJson(true, 200, "OK", []);
});

$router->get("/services", function() {
    responseJson(true, 200, "SERVICES", []);
});

$router->get("/db", function() {
    responseJson(false);
})

$router->get('/upload', function() {
    responseJson(true, 201);
})

$router->get('/cadastro', function() {
    responseJson(true, 203);
})

$router->post('/store', function() {
    responseJson(true, 202);
})

// DB


// EMAIL


// UPLOAD

/**
 * routes
 * The controller must be in the namespace Test\Controller
 * this produces routes for route, route/$id, route/{$id}/profile, etc.
 */

/**
 * This method executes the routes
 */
$router->dispatch();

/*
 * Redirect all errors
 */
if ($router->error()) {
    die('erro');
    $router->redirect("/error/{$router->error()}");
}