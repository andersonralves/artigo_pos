<?php

class DB
{
    private $db = null;

    public function getInstance()
    {
        if (self::$db == $null)
            $db = new PDO('sqlite:database.sqlite3');

        return self::$db;
    }
    
}