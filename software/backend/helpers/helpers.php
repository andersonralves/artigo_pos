<?php

function config($file) {
    
}

function responseJson($success = true, $status = 200, $message = 'OK', $data = [])
{
    header('Content-Type: application/json; charset=utf-8');
    http_response_code($status);

    $result = [
        'success' => true,
        'status_code' => $status,
        'message' => $message,
    ];

    if (!empty($data))
        $result['data'] = $data;

    echo json_encode($result, JSON_UNESCAPED_SLASHES);

    die;
}